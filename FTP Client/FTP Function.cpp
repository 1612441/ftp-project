#include "stdafx.h"
#include "FTP Client.h"

int FTPSocket::Send(string content, bool showMsg = TRUE){
	//in lệnh được gửi lên
	if (showMsg) cout << "ftp> " << content << endl;

	//gửi lệnh lên server
	content+= "\n";
	return ControlSocket.Send(&content[0], content.length());
}

int FTPSocket::Recieve(int& code, bool showMsg = TRUE){
	//nhận phản hồi từ server
	char content[257] = { 0 };
	int t = ControlSocket.Receive(content, 256);	
	if (!t) {
		code = 0;
		return 0;
	}

	//in thông báo phản hồi
	if (showMsg) cout << content;

	//lấy mã phản hồi
	char code_str[4];
	strncpy_s(code_str, content, 3);
	code = atoi(code_str);

	//
	if (code == 421){
		//open();
	}

	return t;
}

//==============================================

void FTPSocket::openControlSocket(){
	//...

	if (ControlSocket.Create() && ControlSocket.Connect(CA2W(Server_IP_Address), 21))
		cout << "Connected to " << Server_IP_Address << endl;
	else {
		cout << "Cannot connect to FTP Server!" << endl;
		return;
	}
}

FTPSocket::FTPSocket(){
	if (AfxSocketInit() == FALSE)
	{
		cout << "Failed to Initialize Sockets!" << endl;
		return;
	}

	//kết nối port 21 của server
	openControlSocket();

	//in câu chào mừng
	int code;
	Recieve(code);
	cout << endl;

	//kết nối ftp server thành công
	if (code == 220){	}
}

FTPSocket::~FTPSocket(){
	//đóng socket
	ControlSocket.Close();
}

void FTPSocket::Logon(){	
	string s;
	int code;
	
	s = "OPTS UTF8 ON\n";
	Send(s, FALSE);
	Recieve(code, FALSE);

	//nhập tên đăng nhập
	cout << "Username: ";
	//getline(cin, s);
	s = "a01";
	s = "user " + s;
	Send(s, FALSE);
	Recieve(code);
	if (code != 331) {	}

	//nhập mật khẩu
	cout << "Password: ";
	//getline(cin, s);
	s = "";
	s = "pass " + s;
	Send(s, FALSE);
	Recieve(code);
	if (code != 230) {	}

}

void FTPSocket::pasv(){
	//gửi lệnh lên server
	Send("PASV");

	//nhận thông báo phản hồi từ server
	char s[128] = { 0 };
	ControlSocket.Receive(s, 127);
	cout << s;

	//đã thành công hay chưa
	char code_str[4];
	strncpy_s(code_str, s, 3);
	if (atoi(code_str) != 227){ return; }

	//tách số địa chỉ IP và port ra khỏi thông báo phản hồi
	string t = s;	
	t = t.substr(t.find('(') + 1);
	t = t.substr(0, t.find(')'));

	//tách lấy số port
	pasv_port = atoi(&t[t.find_last_of(',') + 1]);
	t = t.substr(0, t.find_last_of(','));
	pasv_port += atoi(&t[t.find_last_of(',') + 1]) * 256;
}

CSocket* FTPSocket::openDataConnect(string cmd, bool showCmd = TRUE){
	//mở kênh data trên client
	if (!DataSocket.Create()){
		cout << "Data Connection Fails !!!" << endl;
		return NULL;
	}

	//===============================
	//kết nối với kênh data của server

	if (!pasv_port){ //chế độ active
		UINT port;
		CString ip;
		int code;

		//client lắng nghe tại port N+1
		DataSocket.GetSockName(ip, port);
		DataSocket.Listen(1);

		//gửi số port N+1 cho server
		string s = "port ";
		s += Client_IP_Address;
		s += "." + to_string(port / 256) + "." + to_string(port % 256);
		Send(s, FALSE);
		Recieve(code, FALSE);
		if (code != 200){ return NULL; }
	}
	else{ //chế độ passive

		//kết nối thẳng với port data của server
		if (!DataSocket.Connect(CA2W(Server_IP_Address), pasv_port)){
			cout << "Data Connection Fails!" << endl;
			DataSocket.Close();
			return NULL;
		}
	}

	//===============================
	//GỬI LỆNH LÊN SERVER

	int code;
	Send(cmd, showCmd);
	Recieve(code, showCmd);
	if (code != 150){ 
		DataSocket.Close();
		return NULL; 
	}

	//===============================
	//mở kết nối data với server

	CSocket *Connector;
	if (!pasv_port){ //chế độ active
		Connector = new CSocket();
		//nhận kết nối data với server
		DataSocket.Accept(*Connector);
	}
	else{ //chế độ passive
		Connector = &DataSocket;
	}

	return Connector;
}

void FTPSocket::closeDataConnect(CSocket* Connector, bool showMsg = TRUE){	
	//đóng kết nối data
	if (!pasv_port){
		Connector->Close();
		delete Connector;
	}
	DataSocket.Close();

	//xác nhận
	int code;
	Recieve(code, showMsg);
	if (code != 226){}

	//chuyển về active mode
	pasv_port = 0;
}

//==============================================

void FTPSocket::quit(){
	Send("QUIT");
	int code;
	Recieve(code);
}

void FTPSocket::dir(string path){
	//thiết lập kết nối data với server
	CSocket* Connector = openDataConnect("LIST " + path);
	if (!Connector) return;

	//DOWNLOAD DATA TỪ SERVER

	char a[2] = { 0 };
	while (Connector->Receive(a, 1))
		cout << a;
	
	//đóng kết nối data
	closeDataConnect(Connector);
}

void FTPSocket::ls(string path){
	//thiết lập kết nối data với server
	CSocket* Connector = openDataConnect("NLST " + path);
	if (!Connector) return;

	//DOWNLOAD DATA TỪ SERVER
	char a[2] = { 0 };
	while (Connector->Receive(a, 1))
		cout << a;

	//đóng kết nối data
	closeDataConnect(Connector);
}

void FTPSocket::put(string filename){
	if (filename == ""){
		cout << "Local filename: ";
		getline(cin, filename);
	}

	//mở file cần gửi lên
	ifstream f;
	f.open(local_directory + filename, ios::binary);
	if (!f.is_open()){
		cout << "File not Found!" << endl;
		return;
	}

	//thiết lập kết nối data với server
	CSocket* Connector = openDataConnect("STOR " + filename);
	if (!Connector) return;	

	//UPLOAD DATA LÊN SERVER
	char a[1];
	while (f.read(a, 1))
		Connector->Send(a, 1);
	f.close();

	//đóng kết nối data
	closeDataConnect(Connector);
}

void FTPSocket::get(string filename){
	if (filename == ""){
		cout << "Remote filename: ";
		getline(cin, filename);
	}

	//thiết lập kết nối data với server
	CSocket* Connector = openDataConnect("RETR " + filename);
	if (!Connector) return;

	//DOWNLOAD DATA TỪ SERVER
	ofstream g;
	g.open(local_directory + filename, ios::binary);
	char a[1];
	while (Connector->Receive(a, 1))
		g.write(a, 1);
	g.close();

	//đóng kết nối data
	closeDataConnect(Connector);
}

void FTPSocket::cd(string path){
	Send("CWD " + path");
	int code;
	Recieve(code);
}

void FTPSocket::lcd(string path){
	local_directory = path;
	//...
}

void FTPSocket::dele(string filename){
	if (filename == ""){
		cout << "Remote filename: ";
		getline(cin, filename);
	}

	Send("DELE" + filename);
	int code;
	Recieve(code);
}

void FTPSocket::mkdir(string path){
	if (path == ""){
		cout << "Remote Directory: ";
		getline(cin, path);
	}
	Send("XMKD" + path);
	int code;
	Recieve(code);
}

void FTPSocket::rmdir(string path){
	if (path == ""){
		cout << "Remote Directory: ";
		getline(cin, path);
	}

	Send("XRMD" + path);
	int code;
	Recieve(code);
}

void FTPSocket::pwd(){
	Send("XPWD");
	int code;
	Recieve(code);
}

void FTPSocket::mput(string){

}

void FTPSocket::mget(string filename){
	if (filename == ""){
		cout << "Remote filename: ";
		cin >> filename;
	}

	//thiết lập kết nối data với server
	CSocket* Connector = openDataConnect("NLST " + filename, FALSE);
	if (!Connector) return;

	//lấy danh sách file
	ofstream g;
	g.open("FTP_FileList.ini", ios::binary);
	char a[1];
	while (Connector->Receive(a, 1))
		g.write(a, 1);
	g.close();

	//đóng kết nối data
	closeDataConnect(Connector, FALSE);

	//====================================

	//tải file
	ifstream f;
	f.open("FTP_FileList.ini");
	string s;
	while (!f.eof()){
		getline(f, s);
		if (s == "") break;
		cout << "get " << s << "? ";
		int key = getchar();
		if (key == '\n') get(s);
	}
	f.close();
}

void FTPSocket::mdele(string filename){
	if (filename == ""){
		cout << "Remote filename: ";
		getline(cin, filename);
	}

	//thiết lập kết nối data với server
	CSocket* Connector = openDataConnect("NLST " + filename, FALSE);
	if (!Connector) return;

	//lấy danh sách file
	ofstream g;
	g.open("FTP_FileList.ini", ios::binary);
	char a[1];
	while (Connector->Receive(a, 1))
		g.write(a, 1);
	g.close();

	//đóng kết nối data
	closeDataConnect(Connector, FALSE);

	//====================================

	//xóa file
	ifstream f;
	f.open("FTP_FileList.ini");
	string s;
	while (!f.eof()){
		getline(f, s);
		if (s == "") break;
		cout << "delete " << s << "? ";
		int key = getchar();
		if (key == '\n') dele(s);		
	}
	f.close();
}