﻿#pragma once

#include "resource.h"
#include <wchar.h>
#include <string>
#include <iostream>
#include <fstream>
#include <Windows.h>
using namespace std;
#include "afxsock.h"


class FTPSocket{
	CSocket ControlSocket, DataSocket;
	char* Server_IP_Address = "127.0.0.1";	//địa chỉ IP máy chủ FTP	
	char* Client_IP_Address = "127.0.0.1";	//địa chỉ IP máy khách
	int pasv_port = 0;		//nếu giá trị 0 là chế độ active, khác 0 là passive
	string local_directory = "";

protected:
	int Send(string, bool);		//gửi lệnh lên server
	int Recieve(int& code, bool);	//nhận thông báo phản hồi từ server
	
	void openControlSocket();
	CSocket* openDataConnect(string, bool);
	void closeDataConnect(CSocket*, bool);

public:
	FTPSocket();
	~FTPSocket();
		
	void Logon();		//đăng nhập vào server	
	void pasv();		//chuyển sang chế độ passive
	void quit();

	void dir(string);
	void ls(string);

	void put(string);	//upload file lên server
	void get(string);	//download file từ server	

	void pwd();			//hiển thị đường dẫn hiện tại trên server
	void cd(string);	//thay đổi đường dẫn trên server
	void lcd(string);	//thay đổi đường dẫn ở client

	void dele(string);	//xóa 
	void mkdir(string);	//tạo thư mục mới trên server
	void rmdir(string);	//xóa thư mục rỗng trên server
		
	void mput(string);
	void mget(string);
	void mdele(string);
};
